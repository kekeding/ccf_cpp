#include<iostream>
#include<string>
#include <fstream>
#include <sstream>
#include <iterator>
#include <list>
#include <vector>
#include<algorithm>

using namespace std;
const int N = 110;
int n2, m2, a[N][N], b[N][N];
string s;

void inputFromConsole1_8_1()
{
	cin >> m2 >> n2;
	for (int i = 1; i <= m2; i++)
	for (int j = 1; j <= n2; j++)cin >> a[i][j];	
	cin >> s;
}

void inputFromFile1_8_1()
{
	string line;
	ifstream data("data1_8_1.txt");
	if (data.is_open())
	{
		getline(data, line);
		istringstream iss(line);
		vector<string> results((istream_iterator<string>(iss)), std::istream_iterator<std::string>());
		m2 = std::stoi(results[0]);
		n2 = std::stoi(results[1]);

		for (int i = 1; i <= m2; i++) {
			getline(data, line);
			istringstream iss(line);
			vector<string> results((istream_iterator<string>(iss)), std::istream_iterator<std::string>());
			for (int j = 1; j <= n2; j++) {
				a[i][j] = std::stoi(results[j - 1]);
			}
		}
		getline(data, s);

		data.close();
	}
}

int f(char a, int x, int y, int g) {
	if (a == 'A') if (!g)return y; else return m2 - x + 1;
	if (a == 'B') if (!g)return n2-y+1; else return x;
	if (a == 'C') if (!g)return x; else return n2 - y + 1;
	if (!g)return m2 -x+1; else return y;
}

void work() {
	for (int k = 0; k < s.length(); k++)
	{
		for (int i = 1; i <= m2; i++)
			for (int j = 1; j <= n2; j++)
				b[f(s[k], i, j, 0)][f(s[k], i, j, 1)] = a[i][j];
		if (s[k] == 'A' || s[k] == 'B') swap(m2, n2);
		for (int i = 1; i <= m2; i++)for (int j = 1; j <= n2; j++)a[i][j] = b[i][j];
	}
}
void output() {
	for (int i = 1; i <= m2; i++)
	{
		for (int j = 1; j <= n2; j++)cout << a[i][j] << ' ';	
		cout << endl;
	}
}

int exam1_8_1()
{
	inputFromFile1_8_1();
	work();
	output();
	return 0;
}