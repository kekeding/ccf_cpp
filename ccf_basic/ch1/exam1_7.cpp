#include<iostream>
#include<cstdlib>
using namespace std;
int m, n7, r, s, a[100][100], b[100][100];
void input()
{
	cin >> m >> n7;
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n7; j++)
			cin >> a[i][j];
	cin >> r >> s;
	for (int i = 0; i < r; i++)
		for (int j = 0; j < s; j++)
			cin >> b[i][j];
}
int compute(int x, int y)
{
	int ans = 0;
	for (int i = 0; i < r; i++)
		for (int j = 0; j < r; j++)
			ans += abs(a[x + i][y + j] - b[i][j]);
	return ans;
}
void work(int &x, int &y)
{
	int mindiff = 1000001;
	for(int i=0;i<=m-r;i++)
		for (int j = 0; j <= n7 - s; j++)
		{
			int curr = compute(i, j);
			if (curr < mindiff)
			{
				mindiff = curr;
				x = i;
				y = j;
			}
		}
}
void output(int x, int y)
{
	for (int i = 0; i < r; i++)
	{
		for (int j = 0; j < s; j++)cout << a[x + i][y + j] << " ";
		cout << endl;
	}
}
int exam1_7()
{
	int x, y;
	input();
	work(x, y);
	output(x, y);
	return 0;
}