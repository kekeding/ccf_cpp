#include<iostream>
using namespace std;
void swap(int &a, int &b)
{
	int t = a;
	a = b;
	b = t;
}
int exam1_4()
{
	int x, y;
	cin >> x >> y;
	swap(x, y);
	cout << x << " " << y << endl;
	return 0;
}