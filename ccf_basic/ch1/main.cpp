#include<iostream>
using namespace std;

/*
申明当前项目中需要运行的方法（或函数）
*/
extern int exam1_1_1();
extern int exam1_1_2();
extern long long f(int n);
extern long long C(int n,int m);
extern void output(int n);
extern int exam1_4();
extern int exam1_5();
extern int exam1_6();
extern int exam1_7();
extern int exam1_8_1();

int main()
{
	/*
	注释掉当前不需要测试的方法
	*/
    //exam1_1_1();
	//exam1_1_2();
	//cout<<f(10);
	//cout << C(6,3);
	//output(7);
	//int x = 3; int y = 5;
	//exam1_4();
	//exam1_5();
	//exam1_6();
	//exam1_7();
	exam1_8_1();
	return 0;
}