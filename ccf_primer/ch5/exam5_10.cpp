#include<iostream>
#include<cstring>
using namespace std;
int exam5_10()
{
	int num[11];
	int i;
	memset(num, 0, sizeof(num));
	/*
	如果输入了大于10的i值，不会提示内存越界的错误，但是结果不正确。
	*/
	while (cin >> i && i>0)
	{
		cout << "inputed:"<<i <<endl;
		num[i] = num[i] + 1;
	}
	for (i = 1; i <= 10; i++)
		cout << "第" << i << "号歌手的选票数为： " << num[i] << endl;
	return 0;
}