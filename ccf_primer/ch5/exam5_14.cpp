#include<iostream>
const int MAXN = 100001;
using namespace std;
int exam5_14()
{
	int n, k, i, j;
	float temp, a[MAXN];
	cin >> n;
	for (i = 0; i < n; i++)
		cin >> a[i];
	for (i = 0; i < n; i++)
	{
		k = i;
		for (j = i + 1; j < n; j++)
			if (a[k] > a[j])k = j;
		if (k != j)
		{
			temp = a[i];
			a[i] = a[k];
			a[k] = temp;
		}
	}
	for (i = 0; i < n; i++)
		cout << a[i] << " ";
	return 0;
}