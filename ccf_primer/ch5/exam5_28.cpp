#include<iostream>
#include<cstring>
#define MAXN 100010
using namespace std;
/*
本程序为约瑟夫环问题，数到m的猴子出局，其他猴子留在圈中。
*/
int exam5_28()
{
	int n, m, i, index, k;
	int a[MAXN];
	cin >> n>>m ;
	memset(a, 0, sizeof(a));
	k = 0; index = 0;
	for (i = 1; i <= n - 1; i++)
	{
		while (k<m)
		{
			index++;
			if (index > n) index = 1;
			if (!a[index]) k++;
		}
		a[index] = !a[index]; k = 0;
	}
	for (i = 1; i <= n; i++)
		if (!a[i])cout << i << endl;
	return 0;
}

/*
本程序为约瑟夫环问题变种，数到1的猴子出局，其他猴子留在圈中。
*/
int exam5_28_1()
{
	int n, m, i, index, k;
	int a[MAXN];
	cin >> n >> m;
	memset(a, 0, sizeof(a));
	k = 0; index = 0;
	for (i = 1; i <= n - 1; i++)
	{
		while (k < m)
		{
			index++;
			if (index > n) index = 1;
			if (a[index] == 0)
			{
				k++;
			}

			if (k == 1)
			{
				a[index] = 1;
			}
		}
		//a[index] = !a[index]; 
		k = 0;
	}
	for (i = 1; i <= n; i++)
		if (!a[i])cout << i << endl;
	return 0;
}