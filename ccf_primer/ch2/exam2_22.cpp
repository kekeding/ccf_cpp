#include<cstdio>
#include<iostream>
using namespace std;
int exam2_22()
{
	printf("%d%d%d\n", 9 / 8, 4 * (6 + 3) % 5, (4 * 6 + 3) % 5);
	printf("%d  %d  %d\n", 9 / 8, 4 * (6 + 3) % 5, (4 * 6 + 3) % 5);

	/*
	如果要在printf中输出%符号，需要用两个%%表示
	*/
	printf("9/8=%d  4*(6+3)%%5=%d  (4*6+3\%5)=%d\n",
		9 / 8, 4 * (6 + 3) % 5, (4 * 6 + 3) % 5);
	
	//cout对比printf，直接输出引号内的内容
	cout<<"9/8=%d  4*(6+3)%%5=%d  (4*6+3\%5)=%d\n";

	printf("%d  %d  %d\n", 41 % 6, 41 % (-6), (-41) % 6);
	return 0;
}