#include<iostream>
using namespace std;
int exam2_16()
{
	int a,b,c;
	long long long_a, long_b, long_c;
	long long s;
	/*
	结果溢出，为负值
		*/
	a = 1234567890;
	b = 2345678901;
	c = 3456789012;
	
	cout << "a=" << a <<",b=" << b << ",c=" << c << endl;

	s = (long long)(a + b + c);
	cout << "s=" << s << endl;

	long_a = 1234567890;
	long_b = 2345678901;
	long_c = 3456789012;

	cout << "long_a=" << long_a << ",long_b=" << long_b << ",long_c=" << long_c << endl;

	s = (long long)long_a + long_b + long_c;

	cout << "s=" << s << endl;
	/*
	结果正常
	*/
	a = 1234567890;
	b = 1345678901;
	c = 1456789012;
	
	cout << "a=" << a << ",b=" << b << ",c=" << c << endl;

	s =(long long) a+b+c;
	cout<<"s=" <<s<< endl;
	return 0;
}