#include<iostream>
using namespace std;
int ex2_4_2()
{
	float a, b, c, s;
	//测试时，输入3 4 5
	cin >> a >> b >> c  ;
	cout << a << " " << b << " "<< c << endl;

	//正确
	s = sqrt((a + b + c)*(a + b - c)*(a + c - b)*(b + c - a)) / 4.0;
	cout << s << endl;

	//正确
	s = 1.0/4.0*sqrt((a + b + c)*(a + b - c)*(a + c - b)*(b + c - a));
	cout << s << endl;

	//错误，输出结果0
	s = 1/ 4*sqrt((a + b + c)*(a + b - c)*(a + c - b)*(b + c - a));
	cout << s << endl;
	return 0;
}