/*
申明当前项目中需要运行的方法（或函数）
*/
extern int exam1_1();
extern int ex1_1_2();
extern int exam1_2();
extern int exam1_3();
extern int exam1_4();
extern int exam1_5();
extern int ex1_2_1();
extern int ex1_2_2();
extern int ex1_2_3();
extern int exam1_6();
extern int exam1_7();
extern int exam1_8();
extern int ex1_3_1();
extern int ex1_3_2();
extern int ex1_3_3();


int main()
{
	/*
	注释掉当前不需要测试的方法
	*/
    //exam1_1();
	//ex1_1_2();
	//exam1_2();
	//exam1_3();
	//exam1_4();
	//exam1_5();
	//ex1_2_1();
	//ex1_2_2();
	//ex1_2_3();
	//exam1_6();
	//exam1_7();
	//exam1_8();
	//ex1_3_1();
	//ex1_3_2();
	//ex1_3_3();
	return 0;
}