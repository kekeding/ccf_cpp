#include <iostream>
#include<iomanip>
using namespace std;
int exam1_8()
{
	cout << "9/8=" << 9 / 8 << endl;
	cout << "9.0/8=" << 9.0 / 8 << endl;
	cout<< "9/8.0=" << 9 / 8.0 << endl;
	cout<< "9.0/8.0" << 9.0 / 8.0 << endl;
	cout << "10.0/6.0=" << 10.0 / 6.0 << endl;
	cout<<fixed << setprecision(8) << "10.0 / 6.0=" << 10.0 / 6.0 << endl;
	return 0;
}