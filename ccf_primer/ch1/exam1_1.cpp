﻿/*
介绍一个简单的C++程序的轮廓。
*/

#include <iostream>
using namespace std;

int exam1_1()
{
	//这是一种注释的方式
    //std::cout << "Hello World!\n";
	cout << "Hello World!\n";
	cout << endl;
	/*
	std::cout和cout的效果是一样的。
	*/
	cout << "I love programming.\n";
	std::cout << "I want to eat hambger.\n";

	/*
	这是另外一种注释的方式
	*/
	/*
	The return value for main indicates how the program exited. 
	Normal exit is represented by a 0 return value from main. 
	Abnormal exit is signaled by a non-zero return, but there is no standard for how non-zero codes are interpreted. 
	void main() is prohibited by the C++ standard and should not be used. 
	
	It is also worth noting that in C++, int main() can be left without a return-statement
	, at which point it defaults to returning 0. This is also true with a C99 program.
	*/
	return 0;
}