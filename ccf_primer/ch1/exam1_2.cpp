#include <iostream>
using namespace std;

int exam1_2()
{
	cout << 20 / 7 << endl;
	cout << 20 % 7 << endl;

	/*
	使用英文符号""包含的内容是字符串，原样输出，
	否则是表达式，输出计算结果。
	*/
	cout << "677 / 7 = " << 677 / 7 << endl;
	cout << "677 % 7 = " << 677 % 7 << endl;

	return 0;
}