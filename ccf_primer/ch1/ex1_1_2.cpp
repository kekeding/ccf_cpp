/*
1.1小节练习(2)
熟悉C++程序点基本结构，输出想说的句子。
*/

#include <iostream>
using namespace std;

/*
如果要在ccf_primer解决方案的ch1项目中运行，需要使用main以外的函数名称
，如果独立运行，把Ex1_2改成main就可以了。

*/
int ex1_1_2()
{
	/*
	这个地方容易错的是把中文标点符号“”当做英文标点符号""使用，导致程序编译出错。
	*/
	cout << "我叫马克."<<endl;
	cout << "My name is Mark."<<endl;
	
	return 0;
} 